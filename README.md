# LSYNCD MIRRORS

2023-11-16

## WARNING

This describes my own use of `lsyncd` for keeping files safe on my computers when power is lost [etc].  You are expected to be familiar with Linux.  If in doubt, don't deploy the ideas contained here.

## BACKGROUND

Disk and drive mirroring is commonly used on enterprise computer systems.  A Linux example is `mdadm`.  Also, a number of file systems implement this:  `zfs`, `btrfs` and soon `bcachefs`.  However, these all have a scary learning curve and some "issues".  I have not used any of them, preferring the non-journal `ext2` file system.

Currently, I deploy an earlier version of this `lsyncd` based tool to mirror my `/people` directory [partition] on my main desktop computer;  that's where I keep files that have been created over 40+ years.  It uses `lsyncd` to create near-realtime copies of the directory on `/mir` and `/backup`.  Currently, `/backup` and `/mir` are different types of external drive: Samsung SSD with USB to SATA adapter and Samsung USB drive.  Consuming two USB ports on a laptop  might be restrictive.   

The command line version of `lsyncd` has three modes:  `-direct`, `-rsync` and `-rsyncssh`.  I used the `-direct` mode:

       lsyncd -direct -delay 0 /people /mir/lsyncd/people

However, this approach couldn't handle the root partition.  Many reasons.  In the following, I describe the developments that rectified this.  Now I use the second mode, with a `lua` script.

## IMPLEMENTATION

Here is the command I now use to run `lsyncd` for mirroring the root file:

       sudo lsyncd  root-mir.lsyncd  -pidfile /tmp/root-mir.pid

which is contained in the `root-mir` script; `root-mir.lsyncd` contains "lua" structures:

     settings{
        logfile = "/tmp/root-mir.log",
        nodaemon = false,
        statusFile = "/tmp/root-mir.status"
     }
     sync{
         default.rsync,
         source = "/",
         target = "/mir/lsyncd/root",
         delay = 0,
         exclude = {
             '/proc/*',
             '/sys/*',
             '/dev/*',
             '/run/*',
             '/tmp/*',
             '/usb/*',
             '/mir/*',
             '/backup/*',
        },
        rsync={
             perms=true,
             times=true,
             owner=true,
             group=true,
             one_file_system=true,
             links = true,
             checksum = true,
        }
    }

The `settings{..}` structure allows the `log` and `status` locations to be specified, and `nodaemon`, whether it is daemonised [run in the background] or not.

The `sync{..}` structure contains the `source`, `target` and other optional parameters.  One of the problems with using `lsyncd` for a root file system is that `/proc`, `/sys` and `/dev` do not behave well:  they are special file systems.  The `exclude` element allows these and others to be excluded.

The `rsync{..}` structure allows detailed controls.  The `one_file_system` option says that we don't want to cross device or file system boundaries [not sure it works].  That is why I exlicitly exclude these directories.  Even with this, socket [non regular] files seem to present a problem, and cannot be copied [pending fix].

I  use the same method for `/mir` and `/backup`, which are either mounted on external drives or partitions.  Some kind of a "macro" processor is needed to generate the essentially similar cases [pending].

The when the `root-mir` command is run as a daemon, it takes a while to "rsync" the root file system to `/mir/lsyncd/root`.  You can monitor its progress in this startup phase using the `df` command.  Also, the `/tmp/root-mir.status` [text] file, containing a list of files rsync'd eventually stops growing [or slows].
  
You can see daemons that are running with

     ps -e -o pid,uid,command | grep " [0-9][0-9]*"  |  grep "lsyncd "

Best put in a simple shell script.

## COMPARISONS

After an unexpected reboot [aka rare crash], it is wise to compare the partitions or devices used for copies with the original, really looking for inconsistencies.  This should be done before the `lsyncd` instances are restarted.  I use:

     sudo rsync -ainx --exclude-from=root.exc / /mir/lsyncd/root/

where `n` means dry run [don't change anything], and `i` causes the differences between source and target to be listed.  These are identified from the key:

    <  sent
    >  received
    f  file
    d  directory
    L  symlink
    D  device
    S  special such as sockets or fifo's
    c  changed
    h  hard link
    s  size different
    t  mod time different
    p  perms different
    o  owner different
    g  group different
    u  reserved
    x  extended info different
    a  ACL info different

The `root.exc` file contains a list of files or directories to be excluded:

     /proc/*
     /dev/*
     /sys/*
     /run/*
     /tmp/*
     /usb/*
     /mir/*
     /backup/*

The last three are mount points, to avoid `lsyncd` copying mounted file systems [not sure if needed:  `one_file_system` should do this].

## RSYNC SNAPSHOT

It is sometimes helpful to take a snapshot of the [possibly live] root file system [for example]:

    sudo rsync -ax --exclude-from=root.exc \
       --delete --delete-excluded \
       / /usb/rsync/<name>

Where `/usb` is mounted on an external device.  Your `/etc/fstab` system file should contain entries for `/usb`, `/mir` and `/backup`;  I assume that you understand or can find out about this.  The <name> is the snapshot root name.

## SCRIPTS

I use simple shell scripts to run above commands, and reduce the opportunities for mistakes.  These scripts follow a naming scheme:

     <source>-<destination>.<action>

Here are some of the scripts I use:

     root-mir            run "lsyncd" daemon for root->mir
     root-mir.lsyncd     Lua structure for root->mir
     root-mir.check      compare root with /mir
     root-backup         run "lsyncd" daemon for root->backup
     root-backup.lsyncd  Lua structure for root->backup
     root-backup.check   compare root with /backup
     root.exc            files excluded from root
     root.rsync <name>   rsync root->/usb [offline backup]
     root.check <name>   rsync root->/usb [offline check]
     pslsyncd            run "ps" to list running "lsyncd" daemons

Naturally, you should look at these scripts before any use!  They should be available on a public web site [to be named].

## LSYNCD DEPENDENCIES

The package dependencies on Void Linux for "lsyncd" are just:

    glibc>=2.36_1
    lua53>=5.3.5_4
    rsync>=0

where the `lua` scripting language is used for the `sync{}` and `settings{}` data structures.  The source code for `lsyncd` consists 3922 lines of `C` and 5429 lines of `lua` script.

## LSYNCD DOCUMENTATION

Apart from the `lsyncd` and `rsync` man pages, the main documentation can be found at

    https://lsyncd.github.io/lsyncd

along with the latest source code.  This documentation is vague on things like sockets [non regular files].

## PLAY TIME

As mirroring the the "root" filesystem requires the use of `sudo`, and can be a bit scary, here is a safe "play" example that you can use to try out `lsyncd` on `~/play/` [ie in your home directory], with the mirror in `~/play-mir/`.  First create the `~/play` directory and put some test files there.  Working in the home directory, use the commands:

     mkdir -p play-mir
     lsyncd  play-mir.lsyncd  -pidfile ~/play-mir.pid

where `play-mir.lsyncd` contains:

    settings{
       logfile = "/home/jack/play-mir.log",
       nodaemon = false,
       statusFile = "/home/jack/play-mir.status"
    } 
    sync{
         default.rsync,
         source = "/home/jack/play",
         target = "/home/jack/play-mir",
         delay = 0,
         exclude = {
              'excluded/',
         },
         rsync={
             perms=true,
             times=true,
             owner=true,
             group=true,
             links = true,
             checksum = true,
         }
     }

Sadly, the shell notation `~/jack` doesn't work inside `lua` structures.

You can compare the mirror with the original with

    rsync -ainx --exclude-from=play.exc ~/play/ ~/play-mir/

where `play.exc` contains

    excluded/

The trailing `/` seems important.

The corresponding scripts are:

    play-mir          run lsyncd daemon for ~/play->~/play-mir
    play-mir.lsyncd   Lua structure for ~/play->~/play-mir
    play-mir.check    compare ~/play with ~/play-mir
    play.exc          files excluded from ~/play-mir

## JACK

The `~` directory [ie in jacks home directory], with the mirror in `/mir/lsyncd/jack/`.  The reason I separate out my home directory is that it contains active files from `firefox` etc.  I use the command

    lsyncd jack-mir -pidfile /log/jack-mir.pid
 
where `jack-mir.lsyncd` contains:

    settings{
       logfile = "/log/jack-mir.log",
       nodaemon = false,
       statusFile = "/log/jack-mir.status"
    } 
    sync{
         default.rsync,
         source = "/home/jack",
         target = "/mir/lsyncd/jack",
         delay = 0,
         exclude = {
              'excluded/',
         },
         rsync={
             perms=true,
             times=true,
             owner=true,
             group=true,
             links = true,
             checksum = true,
         }
     }

Again, the shell notation `~` doesn't work inside `lua` structures.

You can compare the mirror with the original with

    rsync -ainx --exclude-from=jack.exc ~ /mir/lsyncd/jack/

where `play.exc` contains

    excluded/

The trailing `/` seems important.

The corresponding scripts are:

     jack-mir            run "lsyncd" daemon for jack->mir
     jack-mir.lsyncd     Lua structure for jack->mir
     jack-mir.check      compare jack with /mir
     jack-backup         run "lsyncd" daemon for jack->backup
     jack-backup.lsyncd  Lua structure for jack->backup
     jack-backup.check   compare jack with /backup
     jack.exc            files excluded from jack
     jack.rsync <name>   rsync jack->/usb [offline backup]
     jack.check <name>   rsync jack->/usb [offline check]


## LOG FILES

The log files, especially `play-mir.log` and `play-mir.status` [etc] can grow with time, and can be periodically be deleted.  Refining the files and directories marked for exclusion can improve things, but some are difficult to specify [eg `Firefox` cache directories].

## CAVEATS

This was developed on `Void Linux`, and should work on other non-systemd distributions.  Life is too short for me to find out whether systemd causes it problems.

## DISCUSSION

The way `lsyncd` is implemented, using the `Linux` `inotifywait` function to detect changes in the file system being watched and `rsyncd` to perform file copies, means that it probably can't mirror rapidly changing systems.  That is why the `exclude` option is used for "cache" directories and `/tmp`.  Temporary directories should in any case be mounted on `ramfs`  or `tmpfs` file systems [if memory allows]. The log files can grow large, but can be deleted at any time without problems.

Have fun!


jack@picamanic
